package com.parspack.simpletv.model;

import java.util.List;

public class VideoModel {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ProgramsModel> getPrograms() {
        return programs;
    }

    public void setPrograms(List<ProgramsModel> programs) {
        this.programs = programs;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    protected String id;
    protected String title;
    protected List<ProgramsModel> programs;
    protected String img;
    protected String url;

}
