package com.parspack.simpletv.model;

import com.parspack.simpletv.model.about.InfoModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainModel {
    public static List<VideoModel> tvList;
    public static InfoModel about ;

    public static List<Object> getTvListForMainFragment(){
        List<Object> lo = new ArrayList<>();
        for (VideoModel vm:tvList) {
            if (vm.getPrograms().size()!=0){
                lo.add(vm);
                for (ProgramsModel pm:vm.getPrograms()) {
                    lo.add(pm);
                }
            }
        }
        return lo;
    }

    public static VideoModel getVideoListById(String mID) {
        for (VideoModel vm:tvList) {
            if (vm.getId().equals(mID))
                return vm;
        }
        return null;
    }
}


