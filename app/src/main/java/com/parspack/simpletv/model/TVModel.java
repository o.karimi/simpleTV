package com.parspack.simpletv.model;


public class TVModel {


    protected String name;
    protected VideoModel chanel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VideoModel getChanel() {
        return chanel;
    }

    public void setChanel(VideoModel chanel) {
        this.chanel = chanel;
    }
}
