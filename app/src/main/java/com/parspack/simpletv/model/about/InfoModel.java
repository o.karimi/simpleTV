package com.parspack.simpletv.model.about;

import com.parspack.simpletv.model.ProgramsModel;

import java.util.List;

/**
 * Created by USER on 10/4/2017.
 */

public class InfoModel {

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    protected String about;
}
