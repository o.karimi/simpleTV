package com.parspack.simpletv;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parspack.simpletv.adapter.Videos_RecyclerViewAdapter;
import com.parspack.simpletv.model.MainModel;
import com.parspack.simpletv.model.VideoModel;
import com.parspack.simpletv.okhttp.ProgramsService;
import com.parspack.simpletv.okhttp.iProgramsService;

import java.util.List;

import okhttp3.Call;


public class MainFragment extends Fragment {

    private View rootView;
    private RecyclerView recyclerView_videos;
    private RecyclerView.Adapter recyclerView_Adapter_videos;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        init();
        if (MainModel.tvList == null) {
            loadData();
        }
        else
        {
            recyclerView_Adapter_videos = new Videos_RecyclerViewAdapter(MainFragment.this.getActivity(), MainModel.getTvListForMainFragment());
            recyclerView_videos.setAdapter(recyclerView_Adapter_videos);
        }

        return rootView;
    }

    private void init() {
        swipeRefreshLayout = rootView.findViewById(R.id.main_swipe_refresh);
        swipeRefreshLayout.setRefreshing(false);
        recyclerView_videos = rootView.findViewById(R.id.main_videos_recycler);
        recyclerView_videos.setNestedScrollingEnabled(false);

        recyclerViewLayoutManager = new LinearLayoutManager(rootView.getContext());

        recyclerView_videos.setLayoutManager(recyclerViewLayoutManager);

        recyclerView_videos.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
    }

    private void loadData() {

        ProgramsService.getInstance().load(new iProgramsService() {
            @Override
            public void onError(Call call, Exception e, int id) {
                // error
            }

            @Override
            public void onResponse(List<VideoModel> response, int id) {
                init();

                recyclerView_Adapter_videos = null;
                recyclerView_Adapter_videos = new Videos_RecyclerViewAdapter(MainFragment.this.getActivity(), MainModel.getTvListForMainFragment());

                recyclerView_videos.setAdapter(recyclerView_Adapter_videos);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
