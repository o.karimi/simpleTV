package com.parspack.simpletv;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parspack.simpletv.model.MainModel;
import com.parspack.simpletv.model.about.InfoModel;
import com.parspack.simpletv.okhttp.AboutService;
import com.parspack.simpletv.okhttp.iAboutService;

import okhttp3.Call;


public class AboutFragment extends Fragment {

    private View rootView;
    private TextView txt;

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about, container, false);
        init();
        return rootView;
    }

    private void init() {
        txt = rootView.findViewById(R.id.about_txt);
        if (MainModel.about == null)
        {
            AboutService.getInstance().about(new iAboutService() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    // error
                }

                @Override
                public void onResponse(InfoModel response, int id) {
                    txt.setText(response.getAbout());
                }
            });
        }
        else
            txt.setText(MainModel.about.getAbout());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
