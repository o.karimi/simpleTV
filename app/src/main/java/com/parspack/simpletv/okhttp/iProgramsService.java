package com.parspack.simpletv.okhttp;

import com.parspack.simpletv.model.VideoModel;

import java.util.List;

public interface iProgramsService {

    public void onError(okhttp3.Call call, Exception e, int id);
    public void onResponse(List<VideoModel> response, int id);
}
