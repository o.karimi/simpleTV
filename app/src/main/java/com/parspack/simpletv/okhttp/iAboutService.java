package com.parspack.simpletv.okhttp;

import com.parspack.simpletv.model.about.InfoModel;

public interface iAboutService {

    public void onError(okhttp3.Call call, Exception e, int id);
    public void onResponse(InfoModel response, int id);
}
