package com.parspack.simpletv.okhttp;

import com.google.gson.Gson;
import com.parspack.simpletv.model.Config;
import com.parspack.simpletv.model.MainModel;
import com.parspack.simpletv.model.about.InfoModel;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import okhttp3.Call;
import okhttp3.Response;

public class AboutService {
    private static AboutService instance;

    public static AboutService getInstance() {
        if (instance == null)
            instance = new AboutService();
        return instance;
    }


    public void about(final iAboutService aboutService){

        OkHttpUtils
                .get()
                .url(Config.GET_ABOUT)
                .build()
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        String string = response.body().string();
                        InfoModel about = new Gson().fromJson(string, InfoModel.class);
                        return about;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        // error
                        aboutService.onError(call, e, id);
                    }

                    @Override
                    public void onResponse(Object response, int id) {
                        InfoModel myResponse = (InfoModel) response;
                        MainModel.about = myResponse;
                        aboutService.onResponse(myResponse,id);
                    }
                });

    }

}
