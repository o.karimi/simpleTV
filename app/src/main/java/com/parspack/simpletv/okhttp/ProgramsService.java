package com.parspack.simpletv.okhttp;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parspack.simpletv.model.Config;
import com.parspack.simpletv.model.MainModel;
import com.parspack.simpletv.model.VideoModel;
import com.parspack.simpletv.model.about.InfoModel;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Response;

public class ProgramsService {
    private static ProgramsService instance;

    public static ProgramsService getInstance() {
        if (instance == null)
            instance = new ProgramsService();
        return instance;
    }


    public void load(final iProgramsService programsService){

        OkHttpUtils
                .get()
                .url(Config.GET_PROGRAMS)
                .build()
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        String string = response.body().string();
                        List<VideoModel> list = parsJson(string);
                        return list;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        // error
                        programsService.onError(call, e, id);
                    }

                    @Override
                    public void onResponse(Object response, int id) {
                        List<VideoModel> myResponse =  (List<VideoModel>) response;
                        MainModel.tvList = myResponse;
                        programsService.onResponse(myResponse,id);
                    }
                });

    }

    private List<VideoModel> parsJson(String string) {
        List<VideoModel> list = new ArrayList<>();
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(string);
        JsonObject obj = element.getAsJsonObject();
        Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            System.out.println(entry.getKey());
            list.add(new Gson().fromJson(entry.getValue(), VideoModel.class));
        }
        return list;
    }

}
