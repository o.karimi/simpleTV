package com.parspack.simpletv.util;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by 1 on 11/27/2017.
 */

public class MessageUtil {

    public static void showMessage(View view , String msg){
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                        .setAction(msg, null).show();
    }

}
