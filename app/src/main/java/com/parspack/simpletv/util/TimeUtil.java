package com.parspack.simpletv.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by 1 on 11/27/2017.
 */

public class TimeUtil {

    public static String getTime(String startTime , String endTime){
        Date sTime = getDateCurrentTimeZone(startTime);
        Date eTime = getDateCurrentTimeZone(endTime);
        if (eTime == null || sTime == null)
            return "0";
        else
            return getDifference(sTime,eTime);

    }

    private static Date getDateCurrentTimeZone(String timestamp) {
        try{
            Long time = Long.valueOf(timestamp);
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(time * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currenTimeZone = (Date) calendar.getTime();
//            return sdf.format(currenTimeZone);
            return currenTimeZone;
        }catch (Exception e) {
        }
        return null;
    }

    public static String getTimeFromTimestamp(String timestamp){
        long longTime =0;
        try{
            Long time = Long.valueOf(timestamp);
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(time * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currenTimeZone = (Date) calendar.getTime();
            longTime = currenTimeZone.getTime();
        }catch (Exception e) {
        }
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        longTime = longTime % daysInMilli;
        long elapsedHours = longTime / hoursInMilli;
        longTime = longTime % hoursInMilli;
        long elapsedMinutes = longTime / minutesInMilli;
        return elapsedHours + " : "+elapsedMinutes;
    }

    private static String getDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return elapsedHours + " : "+elapsedMinutes;
    }

}
