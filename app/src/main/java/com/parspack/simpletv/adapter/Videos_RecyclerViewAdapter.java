package com.parspack.simpletv.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.parspack.simpletv.MainFragment;
import com.parspack.simpletv.PlayerFragment;
import com.parspack.simpletv.R;
import com.parspack.simpletv.model.ProgramsModel;
import com.parspack.simpletv.model.VideoModel;
import com.parspack.simpletv.util.MessageUtil;
import com.parspack.simpletv.util.TimeUtil;

import java.util.List;


/**
 * Created by USER on 10/29/2017.
 */

public class Videos_RecyclerViewAdapter extends RecyclerView.Adapter<Videos_RecyclerViewAdapter.ViewHolder>{

    List<Object> videoList ;
    Activity context;
    View view1;

    public Videos_RecyclerViewAdapter(Activity context, List<Object> videoList ){

        this.videoList = videoList;
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textTitle, textTime;
        public ImageView img;
        public ImageButton btnAction;

        public ViewHolder(View v){

            super(v);

            textTitle = v.findViewById(R.id.recycler_item2_itle);
            textTime = v.findViewById(R.id.recycler_item2_time);
            img = v.findViewById(R.id.recycler_item2_image);
            btnAction = v.findViewById(R.id.recycler_item2_action);
        }
    }

    @Override
    public Videos_RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        view1 = LayoutInflater.from(context).inflate(R.layout.recycler_view_item,parent,false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position){
        if (videoList.get(position) instanceof VideoModel)
            bindVideos( Vholder,  position );
        else if (videoList.get(position) instanceof ProgramsModel)
            bindPrograms( Vholder,  position );
    }

    private void bindVideos(ViewHolder vholder, final int position) {
        final VideoModel vm = (VideoModel)  videoList.get(position);
        vholder.textTitle.setText(vm.getTitle());
        vholder.textTime.setText("");
        vholder.img.setVisibility(View.VISIBLE);
        Glide.with(context).load(vm.getImg())
                .thumbnail(0.5f)
                .override(150, 150)
                .centerCrop()
                .into(vholder.img);

        vholder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // open video player fragment
                PlayerFragment fragment = PlayerFragment.newInstance(vm.getId());
//                FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
                FragmentManager fm = context.getFragmentManager();
                android.app.FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.content_main,fragment);
                ft.addToBackStack("playerFragment");
                ft.commit();
            }
        });
    }

    private void bindPrograms(ViewHolder vholder, final int position) {
        final ProgramsModel vm = (ProgramsModel)  videoList.get(position);
        vholder.textTitle.setText(vm.getTitle());
        vholder.textTime.setText(TimeUtil.getTime(vm.getStart(),vm.getEnd()));
        vholder.img.setVisibility(View.INVISIBLE);

        vholder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MessageUtil.showMessage(view , TimeUtil.getTimeFromTimestamp(vm.getStart())+
                " - "+TimeUtil.getTimeFromTimestamp(vm.getEnd()));
            }
        });
    }

    @Override
    public int getItemCount(){
        return videoList.size();
    }
}
